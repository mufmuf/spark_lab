# Andriy Kurdyukov
# Ege Ögretmen
# Spark Job for cluster. Implements 4.1

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.sql.functions import *
from pyspark.sql.window import Window
import re
from operator import add


sparkSession = SparkSession.builder.appName("experiment1").config("spark.cores.max", 7).getOrCreate() 


userRatings = sparkSession.sparkContext.textFile("../lab1819g2/users_libraries.txt")
# separates the user hash and the paper ids

userRatingsRDD = userRatings.map(lambda line: line.split(';')).map(lambda pair: (pair[0], pair[1].split(',')))


users=userRatingsRDD.groupByKey().count()#a)Number of (distinct) users

papers=userRatingsRDD.flatMap(lambda x:x[1]).map(lambda word:(word,1)).reduceByKey(lambda a, b: a + b).count()   #a)number of (distinct) items

print(users,papers)

data = [('users', users), ('papers', papers)]
df = sparkSession.createDataFrame(data)

df.rdd.repartition(1).saveAsTextFile("../lab1819g2/countedUsersPapers.txt")   

#df.write.format("txt").save("../lab1819g2/df.txt")           #unite in one file


