from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.sql import Row
from pyspark.ml.feature import StopWordsRemover, OneHotEncoder, StringIndexer
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.regression import LabeledPoint

import nltk
#nltk.download('wordnet')
from pyspark.sql import functions as f
from pyspark.sql.window import Window
from pyspark.ml.linalg import Vector, SparseVector, VectorUDT, DenseVector, Vectors
import scipy.sparse as sps

from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.tuning import CrossValidator
from pyspark.ml.tuning import ParamGridBuilder
from pyspark.ml.classification import RandomForestClassifier

import random
import numpy as np
import re
import math
from functools import reduce
from datetime import datetime

sparkSession = SparkSession.builder.appName("experiment6") \
    .config("spark.driver.memory","16g") \
    .config("spark.executor.memory","4g") \
    .config("spark.driver.maxResultSize","8g") \
    .getOrCreate()

## EX 1


stopwords = [line.rstrip('\n') for line in open('../lab1819g2/stopwords_en.txt')] 

random.seed(1)



papersRead = sparkSession.sparkContext.textFile("../lab1819g2/papers.csv")

random.seed(1)

papersRDD = papersRead.map(lambda line: line.split(',')) \
    .map(lambda tokens: (tokens[0], tokens[1], tokens[6], tokens[9]))

fields = [
    StructField("paper_id_orig", IntegerType(), False),
    StructField("paper_type", StringType(), True),
    StructField("pages", IntegerType(), True),
    StructField("year", IntegerType(), True)
]
schema = StructType(fields)
def safeInt(maybeInt):
    try:
        return int(maybeInt)
    except ValueError:
        return None

papersDF = sparkSession.createDataFrame(papersRDD.map(lambda row: Row(
            paper_id_orig=safeInt(row[0]), 
            paper_type=row[1],
            pages=safeInt(row[2]),
            year=safeInt(row[3]))), schema)

# give increasing numbers to the rows
w = Window.orderBy(f.col('paper_id_orig'))
papersDF = papersDF.withColumn('paper_id', f.row_number().over(w))
papersDF.cache()

maxPaperId = papersDF.count()

userRatingsRDD = sparkSession.sparkContext.textFile("../lab1819g2/users_libraries.txt")
# separates the user hash and the paper ids
userRatingsRDD = userRatingsRDD.map(lambda line: line.split(';')) \
    .map(lambda pair: (pair[0], pair[1].split(','))) \
    .flatMapValues(lambda x: x)

fields = [
    StructField("user_id", StringType(), False),
    StructField("paper_id_orig", IntegerType(), False),
    StructField("label", IntegerType(), False)
]
schema = StructType(fields)
userPapersDF = sparkSession.createDataFrame(userRatingsRDD.map(lambda row: Row(
                user_id=row[0],
                paper_id_orig=int(row[1]),
                label=1)), schema)

userPapersDF = userPapersDF.join(papersDF, papersDF.paper_id_orig == userPapersDF.paper_id_orig) \
                .select('user_id', 'paper_id', 'label')

def nonRelevantPapers(relevantPapers, maxPaperId):
    def nonRelevantPapersHelper(relevantPapers, sampleNonRelevantPapers):
        paperCount = len(relevantPapers) - len(sampleNonRelevantPapers)
        sampleNonRelevantPapers += random.sample(range(1, maxPaperId + 1), paperCount)
        commonPapers = set(sampleNonRelevantPapers).intersection(relevantPapers)
        # we should not only check if there are no common papers, we also have to make sure that when we
        # sample again, we don't get duplicate papers from the already sampled portion of non relevant papers
        if len(commonPapers) == 0 and len(sampleNonRelevantPapers) == len(relevantPapers):
            return sampleNonRelevantPapers
        else:
            return nonRelevantPapersHelper(relevantPapers, list(set(sampleNonRelevantPapers).difference(commonPapers)))
        
    return nonRelevantPapersHelper(relevantPapers, [])

nonRelevantPapersUDF = f.udf(nonRelevantPapers, ArrayType(IntegerType(), False))
# collect the paper ids per user
# generate random non relevant paper ids
# explode the list of non relevant paper ids and give them the label '1'
userNonRelevantPapersDF = userPapersDF.groupBy('user_id').agg(f.collect_list('paper_id').alias('paper_ids')) \
    .select('user_id', nonRelevantPapersUDF('paper_ids', f.lit(maxPaperId)).alias('non_relevant_paper_ids')) \
    .select('user_id', f.explode('non_relevant_paper_ids').alias('paper_id'), f.lit(0).alias('label'))

userFeaturesDF = userPapersDF.join(papersDF, 'paper_id') \
    .groupBy('user_id').agg(f.count('paper_id').alias('num_papers'), f.avg('pages').alias('avg_pages'))

dataset = userPapersDF.union(userNonRelevantPapersDF)
dataset = dataset.join(papersDF, 'paper_id').join(userFeaturesDF, 'user_id')

## EX 2

# assuming the papers would date back to 1800s
validYearCondition = (f.col('year') > 1800) & (f.col('year') <= 2018)
# assuming a paper may consist of at most 99 pages
validPagesCondition = (f.col('pages') > 0) & (f.col('pages') < 100)
# our assumptions cover a broad range of data of which it is not possible to perfectly separate the faulty and genuine data
avg_year = papersDF.filter(validYearCondition).agg(f.avg('year')).head()['avg(year)']
avg_pages = papersDF.filter(validPagesCondition).agg(f.avg('pages')).head()['avg(pages)']

dataset = dataset.withColumn('year_imputed', f.when(validYearCondition, dataset.year).otherwise(f.lit(avg_year))) \
    .withColumn('pages_imputed', f.when(validPagesCondition, dataset.pages).otherwise(f.lit(avg_pages))) \
    .fillna({'avg_pages': avg_pages})

# Without using OneHotEncoder, we have to add the dummy variables one by one as below

# papersDF.withColumn('paper_type_article', f.when(papersDF.paper_type == 'article', 1).otherwise(0)) \
#    .withColumn('paper_type_electronic', f.when(papersDF.paper_type == 'electronic', 1).otherwise(0)) \
#    .withColumn('paper_type_misc', f.when(papersDF.paper_type == 'misc', 1).otherwise(0))
# ...

# Using OneHotEncoder 
stringIndexerModel = StringIndexer(inputCol="paper_type", outputCol="paper_type_index").fit(dataset)
td = stringIndexerModel.transform(dataset)
datasetOneHot = OneHotEncoder(inputCol="paper_type_index", outputCol="paper_type_vec", dropLast=False) \
    .transform(td).select('user_id', 
                          'paper_id', 
                          f.col('pages_imputed').alias('pages'), 
                          f.col('year_imputed').alias('year'), 
                          'num_papers', 
                          'avg_pages', 
                          f.col('paper_type_vec').alias('paper_type'), 
                          'label')

# transforms the different columns into a single feature column which is a SparseVector
def featurize(pages, year, num_papers, avg_pages, paper_type):
    def featurize_(pages, year, num_papers, avg_pages, paper_type):
        added_indices = [paper_type.size + i for i in range(4)]
        added_values = [pages, year, num_papers, avg_pages]
        return Vectors.sparse(paper_type.size + 4, np.append(paper_type.indices, added_indices), np.append(paper_type.values, added_values))
    
    return f.udf(featurize_, VectorUDT())(pages, year, num_papers, avg_pages, paper_type)

# features column is a 18-dimension vector where 14 of the dimensions represent one-hot encoding of the paper_type
# and the rest of the dimensions are pages, year, num_papers and avg_pages
dataset1 = datasetOneHot.select('user_id', 'paper_id', featurize('pages', 'year', 'num_papers', 'avg_pages', 'paper_type').alias('features'), 'label')

## EX 3

def toLabeledPoint(row):
    return LabeledPoint(row.label, row.features.toArray())

dataset1rdd = dataset1.select('label', 'features').rdd.map(toLabeledPoint)


test_df=  dataset1rdd.toDF()



#trainErrs = {}

#for regParam in [0.1, 0.01, 0.001]:
#    trainErrs[regParam] = {}
 #   for convergenceTol in [0.01, 0.001]:
 #       trainErrs[regParam][convergenceTol] = {}
 #       for step in [0.5, 1.0, 2.0]:
                        
            # Build the model
#            model = SVMWithSGD.train(dataset1rdd, regParam=regParam, convergenceTol=convergenceTol, step=step)

            # Evaluating the model on training data
#            labelsAndPreds = dataset1rdd.map(lambda p: (p.label, model.predict(p.features)))
#            trainErrs[regParam][convergenceTol][step] = labelsAndPreds.filter(lambda pair: pair[0] != pair[1]).count() / float(dataset1rdd.count())

#with open("metrics", "a+") as f:
#    f.write(str(datetime.now())  )
#    f.write(str(trainErrs))


##EX 4

def splitAndTrain(test_df):
    df_split= test_df.randomSplit([0.8, 0.2], 2) 
    df_train=df_split[0]
    df_test=df_split[1]

    rdd_train = df_train.select('label', 'features').rdd.map(toLabeledPoint)

    rdd_test=  df_test.select('label', 'features').rdd.map(toLabeledPoint)
    model = SVMWithSGD.train(rdd_train, regParam=0.1)

    # Evaluating the model on training data

    predictions1 = df_test.rdd.map(lambda p: (p.label, float(model.predict(p.features))) )

    evaluator = RegressionEvaluator(labelCol="_1", predictionCol="_2", metricName="rmse")
    rmse = evaluator.evaluate(predictions1.toDF())
    with open("metrics_svm", "a+") as f:
        f.write("\n")
        f.write(str(datetime.now()))
        f.write("rmse value with svm:")
        f.write(str(rmse))
        f.write("\n")
        
    return rmse



def splitAndTrainRF(dataset1):
    df_split= dataset1.randomSplit([0.8, 0.2], 2) 
    df_train=df_split[0]
    df_test=df_split[1]
    rf=RandomForestClassifier().setFeaturesCol("features")
    model = rf.fit(df_train)
    
    predictions1 =model.transform(df_test)
    prediction=predictions1.select('label','prediction').rdd.map(lambda p:( p[0],float(p[1]) ) )
    evaluator = RegressionEvaluator(labelCol="_1", predictionCol="_2", metricName="rmse")

    rmse = evaluator.evaluate(prediction.toDF())
    with open("metrics", "a+") as f:
        f.write("\n")
        f.write(str(datetime.now())) 
        f.write("rmse value with rf:")
        f.write(str(rmse))
        f.write("\n")
        
    return rmse


    


#splitAndTrain(test_df)    #split,train the model and determine   uncommented 				    #rmse



#do the same for forests

#splitAndTrainRF(dataset1)    #train and test for random forests



##EX 5

##a)######################
def featurizeN(pages, year, num_papers, avg_pages,avg_volume, paper_type):
    def featurizeN_(pages, year, num_papers, avg_pages, avg_volume,paper_type):
        added_indices = [paper_type.size + i for i in range(5)]
        added_values = [pages, year, num_papers, avg_pages,avg_volume]
        return Vectors.sparse(paper_type.size + 5, np.append(paper_type.indices, added_indices), np.append(paper_type.values, added_values))
    
    return f.udf(featurizeN_, VectorUDT())(pages, year, num_papers, avg_pages,avg_volume, paper_type)


   

# apply the first 4 steps
papers = papersRead.map(lambda line: line.split(',', 13)) \
    .map(lambda line: (line[0], line[-1])) \
    .mapValues(lambda text: re.split("[^A-Za-z_-]", text)) \
    .flatMapValues(lambda x: x) \
    .mapValues(lambda word: re.sub('[_-]', '', word)) \
    .filter(lambda data: len(data[1]) >= 3) \
    .groupByKey() \
    .map(lambda data: Row(paper_id=int(data[0]), paper_words=list(data[1])))

# convert to DataFrame because StopWordsRemover is not #compatible with RDDs
fields = [
    StructField('paper_id', IntegerType(), False),
    StructField('paper_words', ArrayType(StringType()), False)
]
schema = StructType(fields)
papersDF = sparkSession.createDataFrame(papers, schema)

stopWordsRemover = StopWordsRemover(inputCol='paper_words', outputCol='words', stopWords=stopwords)
papersDFStopWordsRemoved = stopWordsRemover.transform(papersDF)


# use the lemmatizer built from the wordnet corpus
lemmatizer = nltk.wordnet.WordNetLemmatizer()
def lemmatizeBuilder(words):
    return list(map(lemmatizer.lemmatize, words))

# build a user defined function to lemmatize each word
lemmatize = f.udf(lemmatizeBuilder, ArrayType(StringType(), False))
papersDFWordsLemmatized = papersDFStopWordsRemoved.select('paper_id', lemmatize(f.col('words')).alias('lemmatized_words'))

# count the number of papers each word is used in.
wordCountsDF = papersDFWordsLemmatized.select('paper_id', f.explode(f.col('lemmatized_words')).alias('word')) \
    .groupBy('word') \
    .agg(f.countDistinct('paper_id').alias('paper_count')) \
    .orderBy(f.col('paper_count').desc())





totalPapers = papersDFWordsLemmatized.count()
totalTermsLimit = 1000

# we need the Window to enumerate the rows
# beware that it starts from 1 to enumerate and not 0 -> this #may be a problem later on
w = Window.orderBy(f.col('paper_count').desc())
terms = wordCountsDF.filter(f.col('paper_count') < totalPapers * 0.1) \
    .filter(f.col('paper_count') > 20) \
    .withColumn('rn', f.row_number().over(w)) \
    .filter(f.col('rn') <= totalTermsLimit) \
    .withColumn('idf', f.log(totalPapers / f.col('paper_count')))

termsList = terms.collect()
termsBroadcast = sparkSession.sparkContext.broadcast(list(map(lambda row: row.word, termsList)))
# we have to broadcast the idfs list to access the values in #all partitions later on
# we will need that to calculate the tf-idf vector
idfsBroadcast = sparkSession.sparkContext.broadcast(list(map(lambda row: row.idf, termsList)))



def words2TermFrequencyVectorHelper(paperWords):
    results =  {}
    for index, term in enumerate(termsBroadcast.value):
        term_count = paperWords.count(term)
        if term_count > 0:
            results[index] = term_count
    
    return SparseVector(totalTermsLimit, results)


words2TermFrequencyVector = f.udf(words2TermFrequencyVectorHelper, VectorUDT())
paper2VecDF = papersDFWordsLemmatized.select('paper_id', words2TermFrequencyVector(f.col('lemmatized_words')).alias('TermFrequencyVector'))


def termFrequencyVector2tfIdfHelper(vec):
    tfs = vec.values
    # data frames cannot be used inside udf, therefore we need #to access the idfs with a broadcast.
    # this is not a problem because list of idfs is not large
    idfs_ = [idfsBroadcast.value[i] for i in vec.indices]
    return SparseVector(vec.size, vec.indices, [x * y for x, y in zip(tfs, idfs_)])
    
termFrequencyVector2tfIdf = f.udf(termFrequencyVector2tfIdfHelper, VectorUDT())




paper2VecDFWithTfIdf = paper2VecDF.withColumn('tf_idf', termFrequencyVector2tfIdf(f.col('TermFrequencyVector')))


df = paper2VecDFWithTfIdf


# separates the user hash and the paper ids
#create rdd with user ratings
    
userRatingsRDDOneUserOnePaperPerRow = userRatingsRDD

fields = [
    StructField("user_id", StringType(), False),
    StructField("paper_id", IntegerType(), False)
]
schema = StructType(fields)
usersLikedPapersDF = sparkSession.createDataFrame(userRatingsRDDOneUserOnePaperPerRow.map(lambda row: Row(user_id=row[0], paper_id=int(row[1]))), schema)


joinedDF=usersLikedPapersDF.join(df,usersLikedPapersDF.paper_id == df.paper_id).select('user_id','TermFrequencyVector')

#summation of the TF-IDF vectors for each user
summedDF=joinedDF.rdd.mapValues(lambda v: v.toArray()).reduceByKey(lambda x, y: x + y).mapValues(lambda x: DenseVector(x)).toDF(["userID", "features"])



j1=usersLikedPapersDF.join(summedDF,usersLikedPapersDF.user_id==summedDF.userID )

j2=j1.join(paper2VecDFWithTfIdf, j1.paper_id==paper2VecDFWithTfIdf.paper_id)

#j2.take(5)

j2=j2.select('user_id','features','tf_idf')



def cosineSimilarity(u, p):
    assert isinstance(u, Vector) and isinstance(p, Vector)
    result = u.dot(p) / (u.norm(2) * p.norm(2))
    if np.isnan(result):
        result=0.0
    return  float(  result )


resultRdd=j2.rdd.map(lambda x: (x[0], cosineSimilarity( x[1] , x[2])))

resultDF=resultRdd.toDF(["userID", "similarity"])


#  add additional col for similarities in the dataset


kkk= datasetOneHot.join(resultDF,resultDF.userID== dataset1.user_id)

dataset2 = kkk.select('user_id', 'paper_id', featurizeN('pages', 'year', 'num_papers', 'avg_pages','similarity', 'paper_type').alias('features'), 'label')    #dataset for rf evaluator

#newDataset=resultDF.join(dataset1,resultDF.userID== dataset1.user_id  ).select('user_id','features','similarity'  ,'label')

newdatasetrdd = dataset2.select('label', 'features').rdd.map(toLabeledPoint)  #add similarity to feature

splitAndTrain(newdatasetrdd.toDF())

splitAndTrainRF(dataset2)





##### b)   adding paper volume as additionalfeature###########


papersWithVolume = papersRead.map(lambda line: line.split(',')) \
    .map(lambda tokens: (tokens[0], tokens[1], tokens[6], tokens[7], tokens[9]))

fields = [
    StructField("paper_id_orig", IntegerType(), False),
    StructField("paper_type", StringType(), True),
    StructField("pages", IntegerType(), True),
    StructField("volume", IntegerType(), True),
    StructField("year", IntegerType(), True)
]
schema = StructType(fields)
def safeInt(maybeInt):
    try:
        return int(maybeInt)
    except ValueError:
        return None

papersDF = sparkSession.createDataFrame(papersWithVolume.map(lambda row: Row(
            paper_id_orig=safeInt(row[0]), 
            paper_type=row[1],
            pages=safeInt(row[2]),
            volume=safeInt(row[3]),
            year=safeInt(row[4]))), schema)

# give increasing numbers to the rows
w = Window.orderBy(f.col('paper_id_orig'))
papersDF = papersDF.withColumn('paper_id', f.row_number().over(w))
#papersDF.cache()

maxPaperId = papersDF.count()




fields = [
    StructField("user_id", StringType(), False),
    StructField("paper_id_orig", IntegerType(), False),
    StructField("label", IntegerType(), False)
]
schema = StructType(fields)
userPapersDF = sparkSession.createDataFrame(userRatingsRDD.map(lambda row: Row(
                user_id=row[0],
                paper_id_orig=int(row[1]),
                label=1)), schema)

userPapersDF = userPapersDF.join(papersDF, papersDF.paper_id_orig == userPapersDF.paper_id_orig) \
                .select('user_id', 'paper_id', 'label')



userNonRelevantPapersDF = userPapersDF.groupBy('user_id').agg(f.collect_list('paper_id').alias('paper_ids')) \
    .select('user_id', nonRelevantPapersUDF('paper_ids', f.lit(maxPaperId)).alias('non_relevant_paper_ids')) \
    .select('user_id', f.explode('non_relevant_paper_ids').alias('paper_id'), f.lit(0).alias('label'))



userFeaturesDF = userPapersDF.join(papersDF, 'paper_id') \
    .groupBy('user_id').agg(f.count('paper_id').alias('num_papers'), f.avg('pages').alias('avg_pages'),f.avg('volume').alias('avg_volume'))


dataset = userPapersDF.union(userNonRelevantPapersDF)
dataset = dataset.join(papersDF, 'paper_id').join(userFeaturesDF, 'user_id')




# assuming the papers would date back to 1800s
validYearCondition = (f.col('year') > 1800) & (f.col('year') <= 2018)
# assuming a paper may consist of at most 99 pages
validPagesCondition = (f.col('pages') > 0) & (f.col('pages') < 100)
# our assumptions cover a broad range of data of which it is not possible to perfectly separate the faulty and genuine data
avg_year = papersDF.filter(validYearCondition).agg(f.avg('year')).head()['avg(year)']
avg_pages = papersDF.filter(validPagesCondition).agg(f.avg('pages')).head()['avg(pages)']



dataset = dataset.withColumn('year_imputed', f.when(validYearCondition, dataset.year).otherwise(f.lit(avg_year))) \
	.withColumn('pages_imputed', f.when(validPagesCondition, dataset.pages).otherwise(f.lit(avg_pages))) \
    .fillna({'avg_pages': avg_pages})




stringIndexerModel = StringIndexer(inputCol="paper_type", outputCol="paper_type_index").fit(dataset)
td = stringIndexerModel.transform(dataset)
datasetOneHot = OneHotEncoder(inputCol="paper_type_index", outputCol="paper_type_vec", dropLast=False) \
    .transform(td).select('user_id', 
                          'paper_id', 
                          f.col('pages_imputed').alias						    ('pages'), 
                          f.col('year_imputed').alias('year'), 
                          'num_papers', 
                          'avg_pages','avg_volume', 
                          f.col('paper_type_vec').alias           				    ('paper_type'), 
                          'label')







dataset1 = datasetOneHot.select('user_id', 'paper_id', featurizeN('pages', 'year', 'num_papers', 'avg_pages','avg_volume', 'paper_type').alias('features'), 'label')




dataset1rdd = dataset1.select('label', 'features').rdd.map(toLabeledPoint)

test_df=  dataset1rdd.toDF()



splitAndTrain(test_df)


splitAndTrainRF(dataset1)
