from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.ml.feature import StopWordsRemover, OneHotEncoder, StringIndexer
from pyspark.sql import functions as f
from pyspark.sql.window import Window
from pyspark.ml.linalg import Vector, SparseVector, VectorUDT, DenseVector, Vectors

import nltk
#nltk.download('wordnet')

import random
import numpy as np
import re
import math
from functools import reduce
from datetime import datetime

sparkSession = SparkSession.builder.appName("experiment6") \
    .config("spark.driver.memory","16g") \
    .config("spark.executor.memory","4g") \
    .config("spark.driver.maxResultSize","8g") \
    .getOrCreate()

## EX 1

papersRDD = sparkSession.sparkContext.textFile("../lab1819g2/papers.csv")

random.seed(1)

papersRDD = papersRDD.map(lambda line: line.split(',')) \
    .map(lambda tokens: (tokens[0], tokens[1], tokens[6], tokens[9]))

fields = [
    StructField("paper_id_orig", IntegerType(), False),
    StructField("paper_type", StringType(), True),
    StructField("pages", IntegerType(), True),
    StructField("year", IntegerType(), True)
]
schema = StructType(fields)
def safeInt(maybeInt):
    try:
        return int(maybeInt)
    except ValueError:
        return None

papersDF = sparkSession.createDataFrame(papersRDD.map(lambda row: Row(
            paper_id_orig=safeInt(row[0]), 
            paper_type=row[1],
            pages=safeInt(row[2]),
            year=safeInt(row[3]))), schema)

# give increasing numbers to the rows
w = Window.orderBy(f.col('paper_id_orig'))
papersDF = papersDF.withColumn('paper_id', f.row_number().over(w))
papersDF.cache()

maxPaperId = papersDF.count()

userRatingsRDD = sparkSession.sparkContext.textFile("../lab1819g2/users_libraries.txt")
# separates the user hash and the paper ids
userRatingsRDD = userRatingsRDD.map(lambda line: line.split(';')) \
    .map(lambda pair: (pair[0], pair[1].split(','))) \
    .flatMapValues(lambda x: x)

fields = [
    StructField("user_id", StringType(), False),
    StructField("paper_id_orig", IntegerType(), False),
    StructField("label", IntegerType(), False)
]
schema = StructType(fields)
userPapersDF = sparkSession.createDataFrame(userRatingsRDD.map(lambda row: Row(
                user_id=row[0],
                paper_id_orig=int(row[1]),
                label=1)), schema)

userPapersDF = userPapersDF.join(papersDF, papersDF.paper_id_orig == userPapersDF.paper_id_orig) \
                .select('user_id', 'paper_id', 'label')

def nonRelevantPapers(relevantPapers, maxPaperId):
    def nonRelevantPapersHelper(relevantPapers, sampleNonRelevantPapers):
        paperCount = len(relevantPapers) - len(sampleNonRelevantPapers)
        sampleNonRelevantPapers += random.sample(range(1, maxPaperId + 1), paperCount)
        commonPapers = set(sampleNonRelevantPapers).intersection(relevantPapers)
        # we should not only check if there are no common papers, we also have to make sure that when we
        # sample again, we don't get duplicate papers from the already sampled portion of non relevant papers
        if len(commonPapers) == 0 and len(sampleNonRelevantPapers) == len(relevantPapers):
            return sampleNonRelevantPapers
        else:
            return nonRelevantPapersHelper(relevantPapers, list(set(sampleNonRelevantPapers).difference(commonPapers)))
        
    return nonRelevantPapersHelper(relevantPapers, [])

nonRelevantPapersUDF = f.udf(nonRelevantPapers, ArrayType(IntegerType(), False))
# collect the paper ids per user
# generate random non relevant paper ids
# explode the list of non relevant paper ids and give them the label '1'
userNonRelevantPapersDF = userPapersDF.groupBy('user_id').agg(f.collect_list('paper_id').alias('paper_ids')) \
    .select('user_id', nonRelevantPapersUDF('paper_ids', f.lit(maxPaperId)).alias('non_relevant_paper_ids')) \
    .select('user_id', f.explode('non_relevant_paper_ids').alias('paper_id'), f.lit(0).alias('label'))

userFeaturesDF = userPapersDF.join(papersDF, 'paper_id') \
    .groupBy('user_id').agg(f.count('paper_id').alias('num_papers'), f.avg('pages').alias('avg_pages'))

dataset = userPapersDF.union(userNonRelevantPapersDF)
dataset = dataset.join(papersDF, 'paper_id').join(userFeaturesDF, 'user_id')

## EX 2

# assuming the papers would date back to 1800s
validYearCondition = (f.col('year') > 1800) & (f.col('year') <= 2018)
# assuming a paper may consist of at most 99 pages
validPagesCondition = (f.col('pages') > 0) & (f.col('pages') < 100)
# our assumptions cover a broad range of data of which it is not possible to perfectly separate the faulty and genuine data
avg_year = papersDF.filter(validYearCondition).agg(f.avg('year')).head()['avg(year)']
avg_pages = papersDF.filter(validPagesCondition).agg(f.avg('pages')).head()['avg(pages)']

dataset = dataset.withColumn('year_imputed', f.when(validYearCondition, dataset.year).otherwise(f.lit(avg_year))) \
    .withColumn('pages_imputed', f.when(validPagesCondition, dataset.pages).otherwise(f.lit(avg_pages))) \
    .fillna({'avg_pages': avg_pages})

# Without using OneHotEncoder, we have to add the dummy variables one by one as below

# papersDF.withColumn('paper_type_article', f.when(papersDF.paper_type == 'article', 1).otherwise(0)) \
#    .withColumn('paper_type_electronic', f.when(papersDF.paper_type == 'electronic', 1).otherwise(0)) \
#    .withColumn('paper_type_misc', f.when(papersDF.paper_type == 'misc', 1).otherwise(0))
# ...

# Using OneHotEncoder 
stringIndexerModel = StringIndexer(inputCol="paper_type", outputCol="paper_type_index").fit(dataset)
td = stringIndexerModel.transform(dataset)
datasetOneHot = OneHotEncoder(inputCol="paper_type_index", outputCol="paper_type_vec", dropLast=False) \
    .transform(td).select('user_id', 
                          'paper_id', 
                          f.col('pages_imputed').alias('pages'), 
                          f.col('year_imputed').alias('year'), 
                          'num_papers', 
                          'avg_pages', 
                          f.col('paper_type_vec').alias('paper_type'), 
                          'label')

# transforms the different columns into a single feature column which is a SparseVector
def featurize(pages, year, num_papers, avg_pages, paper_type):
    def featurize_(pages, year, num_papers, avg_pages, paper_type):
        added_indices = [paper_type.size + i for i in range(4)]
        added_values = [pages, year, num_papers, avg_pages]
        return Vectors.sparse(paper_type.size + 4, np.append(paper_type.indices, added_indices), np.append(paper_type.values, added_values))
    
    return f.udf(featurize_, VectorUDT())(pages, year, num_papers, avg_pages, paper_type)

# features column is a 18-dimension vector where 14 of the dimensions represent one-hot encoding of the paper_type
# and the rest of the dimensions are pages, year, num_papers and avg_pages
dataset1 = datasetOneHot.select('user_id', 'paper_id', featurize('pages', 'year', 'num_papers', 'avg_pages', 'paper_type').alias('features'), 'label')

dataset1.write.save(path="dataset", mode="overwrite")
