from pyspark.sql import SparkSession

from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.tuning import CrossValidator
from pyspark.ml.tuning import ParamGridBuilder

from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.regression import LabeledPoint

from datetime import datetime

## EX 3

sparkSession = SparkSession.builder.appName("experiment6") \
    .config("spark.driver.memory","16g") \
    .config("spark.executor.memory","4g") \
    .config("spark.driver.maxResultSize","8g") \
    .getOrCreate()

dataset1 = sparkSession.read.load('dataset')

def toLabeledPoint(row):
    return LabeledPoint(row.label, row.features.toArray())

dataset1rdd = dataset1.select('label', 'features').rdd.map(toLabeledPoint)

trainErrs = {}

regParams = [0.01]
convergenceTols = [0.001]
steps = [1.0]
for regParam in regParams:
    trainErrs[regParam] = {}
    for convergenceTol in convergenceTols:
        trainErrs[regParam][convergenceTol] = {}
        for step in steps:
                        
            # Build the model
            model = SVMWithSGD.train(dataset1rdd, regParam=regParam, convergenceTol=convergenceTol, step=step)

            # Evaluating the model on training data
            labelsAndPreds = dataset1rdd.map(lambda p: (p.label, model.predict(p.features)))
            trainErrs[regParam][convergenceTol][step] = labelsAndPreds.filter(lambda pair: pair[0] != pair[1]).count() / float(dataset1rdd.count())

with open("metrics_svm", "a+") as f:
    f.write(str(datetime.now()))
    f.write(": ")
    f.write(str(trainErrs))
    f.write("\n")