from pyspark.sql import SparkSession

from pyspark.ml.classification import RandomForestClassifier

from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.tuning import CrossValidator
from pyspark.ml.tuning import ParamGridBuilder

from datetime import datetime

## EX 3

sparkSession = SparkSession.builder.appName("experiment6") \
    .config("spark.driver.memory","16g") \
    .config("spark.executor.memory","4g") \
    .config("spark.driver.maxResultSize","8g") \
    .getOrCreate()

dataset1 = sparkSession.read.load('dataset')

rf = RandomForestClassifier()
grid = ParamGridBuilder().addGrid(rf.numTrees, [20]) \
        .addGrid(rf.maxDepth, [5]).build()
evaluator = RegressionEvaluator()
cv = CrossValidator(estimator=rf, estimatorParamMaps=grid, evaluator=evaluator)
cvModel = cv.fit(dataset1)

with open("metrics_rf", "a+") as f:
    f.write(str(datetime.now()))
    f.write(": ")
    f.write(str(cvModel.avgMetrics))
    f.write("\n")